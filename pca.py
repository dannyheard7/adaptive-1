import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

## load the dataset
train = np.genfromtxt ('digits/train.csv', delimiter=",")
trainlabels = np.genfromtxt ('digits/trainlabels.csv', delimiter=",")

# PCA
data = train.T  # Transposed so the data is in the form examples X features for PCA
mean = np.mean(data, axis=0)
centered_data = data - mean

cov = np.dot(centered_data.T, centered_data) / (centered_data.shape[0] - 1)
#cov = np.cov(centered_data.T)

eigenvalues, eigenvectors= np.linalg.eig(cov)
indexes = eigenvalues.argsort()[::-1] # Sort eigenvalues in descending order
eigenvectors = eigenvectors[:, indexes] # Sort eigenvectors by corresponding eignenvalue

num_principal_components = 5
principal_components = eigenvectors[:, :num_principal_components]
reduced_data = centered_data.dot(principal_components)


for components in [[0, 1 , 2] , [1, 2, 3], [0, 2, 3], [1, 3, 4], [2, 3, 4]]:
    pca_fig = plt.figure(figsize = (10, 8))
    pca_ax = pca_fig.gca(projection = '3d')
    pca_fig.canvas.set_window_title("{}_{}_and_{}_PC".format(components[0] + 1, components[1] + 1, components[2] + 1))
    pca_ax.set_title("Examples from each class plotted in the {}, {} and {} PC dimensions".format(components[0] + 1, components[1] + 1, components[2] + 1))
    pca_ax.set_xlabel("{} PC".format(components[0] + 1))
    pca_ax.set_ylabel("{} PC".format(components[1] + 1))
    pca_ax.set_zlabel("{} PC".format(components[2] + 1))
    pca_fig.tight_layout()

    means_fig = plt.figure(figsize = (8, 5)) # Means could probably be a subplot
    means_ax = means_fig.gca(projection = '3d')
    means_fig.canvas.set_window_title("{}_{}_and_{}_PC_means".format(components[0] + 1, components[1] + 1, components[2] + 1))
    means_ax.set_title("Means of each class in the {}, {}, and {} PC dimensions".format(components[0] + 1, components[1] + 1, components[2] + 1))
    means_ax.set_xlabel("{} PC".format(components[0] + 1))
    means_ax.set_ylabel("{} PC".format(components[1] + 1))
    means_ax.set_zlabel("{} PC".format(components[2] + 1))
    means_fig.tight_layout()
    
    for label in np.unique(trainlabels):
        index = np.where(trainlabels == label)
        label = str(int(label)) + "s"

        data = reduced_data[index, :][0, :]        
        pca_ax.plot(data[:, components[0]], data[:, components[1]], data[:, components[2]], label=label, linestyle='None', marker='.')
        mean = data[:, components].mean(axis=0)
        means_ax.plot([mean[0]], [mean[1]], [mean[2]], linestyle='None', marker='o', label=label)

    handles, labels = pca_ax.get_legend_handles_labels()
    pca_ax.legend(handles, labels)

    handles, labels = means_ax.get_legend_handles_labels()
    means_ax.legend(handles, labels)       

plt.show()