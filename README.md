# Princripal Component Analysis

Ensure the digits folder is in the same folder as the pca.py file, this contains the MINST data files.  

Pyhton version: 3. Required libraries: matplotlib and numpy
Run: 'python pca.py'  

The output will be five graphs showing the examples in the principal axes and another five graphs showing the means of the classes in the princiapl axes.  

# Competitive Learning

Ensure the digits folder is in the same folder as the pca.py file, this contains the MINST data files.  

Pyhton version: 3. Required libraries: matplotlib and numpy  
Run: 'python competitive.py'  

The output will be the list of dead neurons, but the output can be further enhanced using the optinal arguments below.  

Optinal Arguments:  
Add noise to the algorithm: --noise True   
Decay the learning rate: --decay True   
Enable lucky learning: --lucky True   
Display the prototypes: --display_prototypes True   
Display the weight change over time: --display_weight_change True   
Display the correlation matrix: --display_correlation True  