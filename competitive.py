import numpy as np
import numpy.matlib
import math
import matplotlib.pyplot as plt
from random import randint
import argparse


def indetify_dead_units(win_counter):
    win_avg = np.median(win_counter)
    std = np.std(win_counter)
    dead_units = np.where(win_counter < (win_avg - 1.5*std))
    print(len(dead_units))

    dead_units = np.where(win_counter < (np.sum(win_counter) * 0.005))
    return dead_units[0]


def plot_prototypes(N_output, W):
    # for output_index in range(N_output):
    #     output = np.reshape(W[output_index,:],(28,28),order="F")
    #     fig = plt.figure()
    #     plt.title('Output Neuron ' + str(output_index))
    #     plt.imshow(output, cmap = 'inferno')

    num_cols = 3
    num_rows = N_output // num_cols

    f, axarr = plt.subplots(num_rows, num_cols, figsize=(10, 10))
    count = 0

    for x in range(0, num_rows):
        for y in range(0, num_cols):
            output = np.reshape(W[count,:],(28,28),order="F")
            axarr[x, y].imshow(output, cmap = 'inferno' )
            axarr[x, y].set_title('Prototype ' + str(count))
            count += 1

    f.tight_layout()


def plot_weight_change(weight_change):
    fig = plt.figure()
    ax = plt.axes()
    plt.plot(weight_change, linewidth=2.0, label='Weight Change')
    plt.title('Weight change over time')
    plt.yscale('log')
    plt.xscale('log')
    plt.ylabel('Weight change (log scale)')
    plt.xlabel('Example number (log scale)')
    fig.legend()


def plot_correlation(W):
    correlation_matrix = np.corrcoef(W)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(correlation_matrix)
    fig.colorbar(cax)
    plt.xticks(np.arange(0, len(W), 1.0))
    plt.yticks(np.arange(0, len(W), 1.0))
    plt.title('Correlation Between Neurons')

    plt.show()


def competitive_learning(use_noise=False, display_prototypes=False, display_weight_change=False, display_correlation=False, decay_learning_rate=False, lucky_learning=False):
    train = np.genfromtxt ('digits/train.csv', delimiter=",")
    trainlabels = np.genfromtxt ('digits/trainlabels.csv', delimiter=",")
    [n,m] = np.shape(train) # number of pixels and number of training data

    eta = 0.05 # learning rate
    N_output = 15 # Number of output neurons
    iterations = 40000

    w_init = 1
    W = w_init * np.random.rand(N_output, n)
    normW = np.sqrt(np.diag(W.dot(W.T)))         # Weight normalisation
    normW = normW.reshape(N_output,-1)            
    W = W / normW           

    alpha = 0.5 # Weight change over time
    weight_change = np.ones((iterations )) * .25
    counter = np.zeros((N_output))              # counter for the winner neurons
    
    for i in range(iterations):
        index = randint(0, m - 1)
        example = train[:,index]
        
        output = W.dot(example) / N_output
        output = output.reshape(output.shape[0], -1)  

        if use_noise:
            noise = np.random.rand(N_output,1) / 200 # Noise
            output = output + noise

        output_max_index = np.argmax(output)

        if iterations % 100 and decay_learning_rate:
            eta = eta - (eta * 0.03)
        
        dw = eta * (example.T - W[output_max_index,:]) 
        W[output_max_index, :] = W[output_max_index, :] + dw

        if lucky_learning:
            loosers = np.arange(len(W)) != output_max_index
            dw_loosers = (eta * 0.00005) * (example.T - W[loosers, :])
            W[loosers, :] = W[loosers, :] + dw_loosers
        
        weight_change[i] = weight_change[i-1] * (alpha + dw.dot(dw.T)*(1-alpha))
        counter[output_max_index] += 1

    dead_units = indetify_dead_units(counter)
    print("There are {} dead units: {}".format(len(dead_units), dead_units))

    if display_prototypes:
        plot_prototypes(N_output, W)
    if display_weight_change:
        plot_weight_change(weight_change)
    if display_correlation:
        plot_correlation(W)
    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--noise', type=bool)
    parser.add_argument('--decay', type=bool)
    parser.add_argument('--lucky', type=bool)
    parser.add_argument('--display_prototypes', type=bool)
    parser.add_argument('--display_weight_change', type=bool)
    parser.add_argument('--display_correlation', type=bool)

    args = parser.parse_args()

    competitive_learning(args.noise, args.display_prototypes, args.display_weight_change, args.display_correlation, args.decay, args.lucky)

